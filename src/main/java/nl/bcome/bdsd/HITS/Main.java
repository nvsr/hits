/*
 * Copyright (c) 2019, Niek Visser
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package nl.bcome.bdsd.HITS;

import nl.bcome.bdsd.HITS.mappers.HitsMapReduce;

import java.util.Map;

public class Main {
    public static void main(String[] args) throws Exception {
        HitsMapReduce hitsMapReduce = new HitsMapReduce("input.txt");
        // Read file and assign incoming (hubs) pages to receivers (authorities)
        Map<String, Page> incomingPages = hitsMapReduce.map();

        /*
         *  Iteration 1:
         *      A: h0.5 - a0.24
         *      B: h0.25 - a0.49
         *      C: h0.25 - a0.49
         *      D: h0.5 - a0.49
         *      E: a0.62 - a0.49
         *
         *  Iteration 2:
         *      A: h0.46 - a0.31
         *      B: h0.19 - a0.37
         *      C: h0.19 - a0.56
         *      D: h0.46 - a0.56
         *      E: a0.71 - a0.37
         *
         *  Iteration 3:
         *      A: h0.44 - a0.35
         *      B: h0.16 - a0.32
         *      C: h0.16 - a0.58
         *      D: h0.44 - a0.58
         *      E: a0.75 - a0.32
         *
         *  Iteration 4:
         *      A: h0.44 - a0.37
         *      B: h0.15 - a0.3
         *      C: h0.15 - a0.59
         *      D: h0.44 - a0.59
         *      E: a0.76 - a0.3
         *
         *  Iteration 5:
         *      A: h0.43 - a0.37
         *      B: h0.14 - a0.29
         *      C: h0.14 - a0.59
         *      D: h0.43 - a0.59
         *      E: a0.77 - a0.29
         *
         *  Iteration 6:
         *      A: h0.43 - a0.38
         *      B: h0.14 - a0.28
         *      C: h0.14 - a0.59
         *      D: h0.43 - a0.59
         *      E: a0.77 - a0.28
         */
        Map<String, Page> result = hitsMapReduce.reduce(incomingPages, 6);

    }
}
