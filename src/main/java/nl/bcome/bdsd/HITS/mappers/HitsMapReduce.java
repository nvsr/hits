/*
 * Copyright (c) 2019, Niek Visser
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package nl.bcome.bdsd.HITS.mappers;

import nl.bcome.bdsd.HITS.Page;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class HitsMapReduce {
    private String file;
    private boolean debug = true;

    public HitsMapReduce(String file) {
        this.file = file;
    }

    /**
     * Read input file and assign incoming neighbors
     *
     * @return {@code HashMap} with
     * @throws IOException Failure to read the input file
     */
    public HashMap<String, Page> map() throws IOException {
        HashMap<String, Page> tmp = new HashMap<>();
        Files.lines(Paths.get(file)).forEach(s -> {
            StringTokenizer tok = new StringTokenizer(s);

            // Grab page names
            String inToken = tok.nextToken();
            String outToken = tok.nextToken();

            // Turn them into objects
            Page in = tmp.getOrDefault(inToken, new Page(inToken, 1, 1));
            Page out = tmp.getOrDefault(outToken, new Page(outToken, 1, 1));

            if (this.debug) System.out.println("Mapping " + in + " as incoming for " + out + " (and vice-versa)");

            // Map in and outgoing
            in.addOutgoing(out);
            out.addIncoming(in);

            // Add to tmp
            tmp.put(out.getName(), out);
            tmp.put(in.getName(), in);
        });
        return tmp;
    }

    /**
     * Calculate auth and hub scores
     *
     * @param pages      Map of pages
     * @param iterations number of iterations
     * @return Map of pages with their scores
     */
    public Map<String, Page> reduce(Map<String, Page> pages, int iterations) {
        for (int i = 0; i < iterations; i++) {
            double norm = 0;
            Set<String> g = pages.keySet();

            // update all authority values first
            for (String p : g) {
                Page pp = pages.get(p);
                pp.setAuth(0);

                // p.incomingNeighbors is the set of pages that link to p
                for (Page q : pp.getIncoming()) {
                    pp.setAuth(
                            pp.getAuth() + q.getHub()
                    );
                    // Update in list
                    pages.put(p, pp);
                }
                // calculate the sum of the squared auth values to normalise
                norm += Math.pow(pp.getAuth(), 2);
            }
            norm = Math.sqrt(norm);

            // update the auth scores
            for (String p : g) {
                Page pp = pages.get(p);
                // normalise the auth values
                pp.setAuth(
                        pp.getAuth() / norm
                );

                // Update in list
                pages.put(p, pp);
            }
            norm = 0;

            // then update all hub values
            for (String p : g) {
                Page pp = pages.get(p);
                pp.setHub(0);
                for (Page r : pp.getOutgoing()) {
                    pp.setHub(
                            pp.getHub() + r.getAuth()
                    );
                    // Update in list
                    pages.put(p, pp);
                }
                // calculate the sum of the squared hub values to normalise
                norm += Math.pow(pp.getHub(), 2);
            }
            norm = Math.sqrt(norm);

            // then update all hub values
            for (String p : g) {
                Page pp = pages.get(p);
                // normalise the hub values
                pp.setHub(
                        pp.getHub() / norm
                );

                // Update in list
                pages.put(p, pp);
                if (this.debug) System.out.println(
                        String.format("Iteration: %d - Page: %s - Auth: %f Hub: %f", i+1, pp.getName(), pp.getAuth(), pp.getHub())
                );
            }
        }
        return pages;
    }
}